# Yatzy assignment

This Yatzy project is the second assignment for the course 
"Objektorienterad Programmering med Java" at Jensen Yrkeshögskola. 
It is a single student assignment.

## Project execution

The basic requirement of this project is  a working Yatzy game with 
Graphical User Interface in swing.

The project was extended with the exploration of some design patterns,
enums and Java generics. Patterns used are:

* Model-View-Control
* Strategy
* Factory
* Builder
* Observer

I have also tried some other interesting language features like streams and some basic functional programming.

## Some of the content ##

Besides code there is an IntelliJ project directory, java doc and a pdf with some messy outdated UML.

If anyone is interested in some fun, maybe overly complicated code, have a look at my Yatzy rules implementation. Its a enum-based factory with a functional interface where I use lambda expressions to define the score-function as an object for each enum. When creating a new score box the stored score function is passed to the constructor. The key here is that constants are not considered part of method parameters in the lambda expressions. Different helper methods with different number of parameters can be used and still match the functional interface if some arguments are constants.

## Build from command line ##

Build instructions tested in OS X terminal. Java 8 required.

1. Create an empty directory and clone the git repo:  
"git clone https://janeriksson@bitbucket.org/janeriksson/yatzy.git"
2. Move into the top directory. Create a class target directory:  
"cd yatzy"  
"mkdir classes"
3. Compile into the new class diretcory:  
"javac -d classes @files.txt"
4. Run the program. Directory src is included to get dice icons:  
"java -cp classes:src games.yatzy.YatzyMain"