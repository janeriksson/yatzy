package games.yatzy.test;

import games.yatzy.rules.ScoreCalculator;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Unit tests for the score calculations.
 *
 *
 * @author Jan Eriksson
 * @version 1.0
 * @since 14/02/16
 */
public class ScoreCalculatorTest {

  @Test
  public void sumOfNsTest() {
    Assert.assertEquals("Wrong sum.", 1, ScoreCalculator.sumOfNs(1,new int[] {1,2,3,4,5}));
    Assert.assertEquals("Wrong sum.", 2, ScoreCalculator.sumOfNs(1,new int[] {1,2,3,1,5}));
    Assert.assertEquals("Wrong sum.", 18, ScoreCalculator.sumOfNs(6,new int[] {1,6,3,6,6}));
  }

  @Test
  public void nSameTest() {
    Assert.assertEquals("Wrong sum from nSame.", 10,
        ScoreCalculator.nSame(2,6,new int[] {5,5,3,4,5}));
    Assert.assertEquals("Wrong sum from nSame.", 9,
        ScoreCalculator.nSame(3,6,new int[] {3,2,3,4,3}));
    Assert.assertEquals("Wrong sum from nSame.", 0,
        ScoreCalculator.nSame(3,6,new int[] {4,2,5,4,5}));
    Assert.assertEquals("Wrong sum from nSame.", 20,
        ScoreCalculator.nSame(4,6,new int[] {5,5,5,5,5}));
  }

  @Test
  public void nSameYahtzeeTest() {
    Assert.assertEquals("Wrong sum from nSameYahtzee.", 22,
        ScoreCalculator.nSameYahtzee(2,6,new int[] {5,5,3,4,5}));
    Assert.assertEquals("Wrong sum from nSameYahtzee.", 15,
        ScoreCalculator.nSameYahtzee(3,6,new int[] {3,2,3,4,3}));
    Assert.assertEquals("Wrong sum from nSameYahtzee.", 0,
        ScoreCalculator.nSameYahtzee(3,6,new int[] {4,2,5,4,5}));
    Assert.assertEquals("Wrong sum from nSameYahtzee.", 25,
        ScoreCalculator.nSameYahtzee(4,6,new int[] {5,5,5,5,5}));
  }

  @Test
  public void twoPairTest() {
    Assert.assertEquals("Wrong sum from twoPair.", 18,
        ScoreCalculator.twoPair(new int[] {5,5,4,4,5}));
    Assert.assertEquals("Wrong sum from twoPair.", 18,
        ScoreCalculator.twoPair(new int[] {3,5,4,4,5}));
    Assert.assertEquals("Wrong sum from twoPair.", 0,
        ScoreCalculator.twoPair(new int[] {3,5,4,4,1}));
  }

  @Test
  public void threePairTest() {
    Assert.assertEquals("Wrong sum from threePair.", 0,
        ScoreCalculator.threePair(new int[] {5,5,4,4,5,5}));
    Assert.assertEquals("Wrong sum from threePair.", 30,
        ScoreCalculator.threePair(new int[] {6,6,4,4,5,5}));
  }

  @Test
  public void fullHouseTest() {
    Assert.assertEquals("Wrong sum from fullHouse.", 23,
        ScoreCalculator.fullHouse(new int[] {5,5,4,4,5,}));
    Assert.assertEquals("Wrong sum from fullHouse.", 11,
        ScoreCalculator.fullHouse(new int[] {1,1,4,4,1,}));
    Assert.assertEquals("Wrong sum from fullHouse.", 0,
        ScoreCalculator.fullHouse(new int[] {5,5,5,4,5,}));
  }

  @Test
  public void fullHouseYahtzeeTest() {
    Assert.assertEquals("Wrong sum from fullHouse.", 25,
        ScoreCalculator.fullHouseYahtzee(new int[] {5,5,4,4,5,}));
    Assert.assertEquals("Wrong sum from fullHouse.", 25,
        ScoreCalculator.fullHouseYahtzee(new int[] {1,1,4,4,1,}));
    Assert.assertEquals("Wrong sum from fullHouse.", 0,
        ScoreCalculator.fullHouseYahtzee(new int[] {5,5,5,4,5,}));
  }

  @Test
  public void towerTest() {
    Assert.assertEquals("Wrong sum from tower.", 28,
        ScoreCalculator.tower(new int[] {5,5,4,4,5,5}));
    Assert.assertEquals("Wrong sum from tower.", 12,
        ScoreCalculator.tower(new int[] {1,1,4,4,1,1}));
    Assert.assertEquals("Wrong sum from tower.", 0,
        ScoreCalculator.tower(new int[] {5,5,5,5,5,5}));
  }


  @Test
  public void villaTest() {
    Assert.assertEquals("Wrong sum from villa.", 27,
        ScoreCalculator.villa(new int[] {5,5,4,4,4,5}));
    Assert.assertEquals("Wrong sum from villa.", 15,
        ScoreCalculator.villa(new int[] {4,1,4,4,1,1}));
    Assert.assertEquals("Wrong sum from villa.", 0,
        ScoreCalculator.villa(new int[] {5,5,5,5,5,5}));
  }

  @Test
  public void smallStraightTest() {
    Assert.assertEquals("Wrong sum from smallStraight.", 15,
        ScoreCalculator.smallStraight(new int[] {5,1,2,3,4}));
    Assert.assertEquals("Wrong sum from smalleStright.", 0,
        ScoreCalculator.smallStraight(new int[] {1,2,3,4,6}));
  }

  @Test
  public void smallStraightYahtzeeTest() {
    Assert.assertEquals("Wrong sum from smallStraightYahtzee.", 30,
        ScoreCalculator.smallStraightYahtzee(new int[] {5,1,2,3,4}));
    Assert.assertEquals("Wrong sum from smallStrightYahtzee.", 30,
        ScoreCalculator.smallStraightYahtzee(new int[] {4,3,2,5,5}));
    Assert.assertEquals("Wrong sum from smalleStrightYahtzee.", 0,
        ScoreCalculator.smallStraightYahtzee(new int[] {5,2,2,4,6}));
  }

  @Test
  public void bigStraightTest() {
    Assert.assertEquals("Wrong sum from bigStraight.", 20,
        ScoreCalculator.bigStraight(new int[] {5,6,2,3,4}));
    Assert.assertEquals("Wrong sum from bigStright.", 20,
        ScoreCalculator.bigStraight(new int[] {4,6,2,5,3}));
    Assert.assertEquals("Wrong sum from bigeStright.", 0,
        ScoreCalculator.bigStraight(new int[] {5,2,3,4,5}));

    // Maxi yatzy case, 6 dice...
    Assert.assertEquals("Wrong sum from bigStraight.", 20,
        ScoreCalculator.bigStraight(new int[] {5,6,2,3,4,6}));
    Assert.assertEquals("Wrong sum from bigStright.", 20,
        ScoreCalculator.bigStraight(new int[] {4,6,2,5,3,1}));
  }

  @Test
  public void bigStraightYahtzeeTest() {
    Assert.assertEquals("Wrong sum from bigStraightYahtzee.", 40,
        ScoreCalculator.bigStraightYahtzee(new int[] {5,1,2,3,4}));
    Assert.assertEquals("Wrong sum from bigStrightYahtzee.", 40,
        ScoreCalculator.bigStraightYahtzee(new int[] {4,6,2,5,3}));
    Assert.assertEquals("Wrong sum from bigeStrightYahtzee.", 0,
        ScoreCalculator.bigStraightYahtzee(new int[] {5,3,2,4,4}));
  }

  @Test
  public void fullStraightTest() {
    // Maxi yatzy case, 6 dice...
    Assert.assertEquals("Wrong sum from fullStraight.", 0,
        ScoreCalculator.fullStraight(new int[] {5,6,2,3,4,6}));
    Assert.assertEquals("Wrong sum from fullStraight.", 21,
        ScoreCalculator.fullStraight(new int[] {4,6,2,5,3,1}));
  }

  @Test
  public void totalSumTest() {
    // Maxi yatzy case, 6 dice...
    Assert.assertEquals("Wrong sum.", 26,
        ScoreCalculator.totalSum(new int[] {5,6,2,3,4,6}));
    Assert.assertEquals("Wrong sum.", 20,
        ScoreCalculator.totalSum(new int[] {4,6,2,5,3}));
  }

  @Test
  public void yahtzyTest() {
    // Maxi yatzy case, 6 dice...
    Assert.assertEquals("Wrong sum from yatzy.", 50,
        ScoreCalculator.yatzy(50, new int[] {3,3,3,3,3}));
    Assert.assertEquals("Wrong sum from yatzy.", 0,
        ScoreCalculator.yatzy(50, new int[] {3,3,1,3,3}));
    Assert.assertEquals("Wrong sum from yatzy.", 100,
        ScoreCalculator.yatzy(100,new int[] {6,6,6,6,6,6}));
  }

  @Test
  public void bonusTest() {
    // Maxi yatzy case, 6 dice...
    Assert.assertEquals("Wrong sum from bonus.", 50,
        ScoreCalculator.yatzyBonus(25, 50, new int[] {5,5,5,5,5}));
    Assert.assertEquals("Wrong sum from bonus.", 50,
        ScoreCalculator.yatzyBonus(25,50, new int[] {6,6,6,6,6}));
    Assert.assertEquals("Wrong sum from bonus.", 0,
        ScoreCalculator.yatzyBonus(25,50, new int[] {4,5,5,5,5}));
  }
}