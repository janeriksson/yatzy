package games.yatzy.view;

import games.util.GenericObserver;
import games.yatzy.model.ScoreInterface;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * <p>
 *   A JPanel with the score names as Labels
 * </p>
 *
 * @author Jan Eriksson
 * @version 1.0
 * @since 09/12/15
 */
public class ScoreColumn extends JPanel implements GenericObserver<ScoreInterface> {

  String currentPlayer;
  private Map<String, JLabel> scoreLabels;
  private final Dimension prefSize = new Dimension(150,20);

  public ScoreColumn(ScoreInterface scoreInterface) {

    List<String>  allScores = scoreInterface.getAllScores();
    this.setLayout(new GridLayout(0,1));
    scoreLabels = new HashMap<>();
    this.add(Box.createRigidArea(prefSize));
    for (String boxId : allScores) {
      JLabel jLabel = new JLabel(boxId, SwingConstants.CENTER);
      Font font = jLabel.getFont();
      Font boldFont = new Font(font.getFontName(), Font.BOLD, font.getSize());
      jLabel.setFont(boldFont);
      jLabel.setPreferredSize(prefSize);
//      jLabel.setHorizontalTextPosition(JLabel.RIGHT);
      this.add(jLabel);
      scoreLabels.put(boxId, jLabel);
    }

    scoreInterface.registerObserver(this);
  }

  public void setCurrentPlayer(String currentPlayer) {
    this.currentPlayer = currentPlayer;
  }

  /**
   * Updates the current score selection based on current players available scores.
   *
   */
  @Override
  public void update(ScoreInterface subjectRef) {
    for (JLabel label : scoreLabels.values()) {
      label.setEnabled(false);
    }

    String playerName = currentPlayer;
    java.util.List<String> availSelect = subjectRef.getAvailableScores(playerName);

    for (String boxId : availSelect) {
      JLabel label = scoreLabels.get(boxId);
      label.setEnabled(true);
    }
  }
}
