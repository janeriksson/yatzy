package games.yatzy.view;

import games.util.GenericObserver;
import games.yatzy.model.GameState;

import javax.swing.*;

/**
 * Listen to gamestate and display current game message.
 *
 * @author Jan Eriksson
 * @version 1.0
 * @since 10/12/15
 */
public class InfoPanel extends JPanel implements GenericObserver<GameState> {

  JLabel jLabel;

  public InfoPanel(GameState gameState) {
    jLabel = new JLabel("Lets play some Yatzy! Click on the dice are to roll active dice. " +
        "Click a die to store it. Select a score box when done.");
    this.add(jLabel);
    gameState.registerObserver(this);
  }


  @Override
  public void update(GameState subjectRef) {
    jLabel.setText(subjectRef.getStateMessage());
    if (subjectRef.isGameEnd()) {
      String winner = subjectRef.getWinner();
      YatzyGui.gameMessage("Game ends with " + winner + " as the winner!");
    }
  }
}

