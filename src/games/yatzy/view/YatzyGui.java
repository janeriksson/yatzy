package games.yatzy.view;

import games.util.GenericObserver;
import games.util.DiceHandler;
import games.yatzy.control.GameControl;
import games.yatzy.model.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

/**
 * <p>
 * GUI wrapper for any kind of Yatzy game.
 * Built from four separate panels: Score, Dice, Buttons and Info.
  * </p>
 *
 * Created by Jan Eriksson on 03/11/15.
 */

public class YatzyGui implements GenericObserver<GameState> {

  private JFrame jFrame;

  // JPanel objects
  private ScoreColumn selectionColumn;
  private PlayerColumn currentPlayerColumn;
  private Map<String,PlayerColumn> playerColumns;
  private DicePanel dicePanel;
  private InfoPanel infoPanel;

  /**
   * Creates all the panels and places them in a GridBagLayout.
   *
   * @param gameControl Controller.
   * @param scoreInterface Score model.
   * @param diceHandler Dice model.
   * @param gameState State model.
   */
  public YatzyGui(GameControl gameControl, ScoreInterface scoreInterface,
                  DiceHandler diceHandler, GameState gameState) {

    jFrame = new JFrame("Yatzy");
    jFrame.setLayout(new GridBagLayout());
    jFrame.setVisible(true);
    jFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

    infoPanel = new InfoPanel(gameState);

    GridBagConstraints c = new GridBagConstraints();
    c.gridx = 0;
    c.gridy = 1;
    c.gridwidth = 1;
    c.fill = GridBagConstraints.BOTH;
    selectionColumn = new ScoreColumn(scoreInterface);
    selectionColumn.setCurrentPlayer(gameState.getCurrentPlayer().getName());
    jFrame.add(selectionColumn,c);

    c.gridwidth = 1;
    c.gridx++;
    playerColumns = new HashMap<>();
    for (Player player : gameState.getPlayers()) {
      currentPlayerColumn = new PlayerColumn(player.getName(), scoreInterface,
          gameControl, gameState);
      playerColumns.put(player.getName(),currentPlayerColumn);
      jFrame.add(currentPlayerColumn,c);
      c.gridx++;
    }

    c.gridy = 1;
    c.gridwidth = 10;
    dicePanel = new DicePanel(gameControl, diceHandler, gameState);
    jFrame.add(dicePanel,c);
    dicePanel.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
        if (gameState.isRollingAllowed()) {
          dicePanel.moveActiveDice(diceHandler);
          gameControl.rollActiveDice();
          jFrame.pack();
        }
      }
    });

    c.gridx = 0;
    c.gridy = 0;
    c.gridwidth = 0;
    c.fill = GridBagConstraints.BOTH;
    jFrame.add(infoPanel,c);

    jFrame.pack();

    gameState.registerObserver(this);
  }
  /**
   * <p>
   * Unsets current player column before getting next player.
   * Set current player to actual current player according to GameState.
   * Mark current player column.
   * <br>
   * Enable/disable roll and score button.
   * </p>
   *
   * @param subjectRef Game state model.
   */
  @Override
  public void update(GameState subjectRef) {
    currentPlayerColumn.setBorder(BorderFactory.createEmptyBorder());
    currentPlayerColumn.setActive(false);
    String currentPlayer = subjectRef.getCurrentPlayer().getName();
    currentPlayerColumn = playerColumns.get(currentPlayer);
    currentPlayerColumn.setBorder(BorderFactory.createEtchedBorder());
    selectionColumn.setCurrentPlayer(currentPlayer);
    currentPlayerColumn.setActive(true);
    jFrame.pack();
  }

  /**
   * Static simple message dialog for use from anywhere.
   * @param message Message to be displayed.
   */
  public static void gameMessage(String message) {
    JOptionPane.showMessageDialog(null, message, "Game Message",
        JOptionPane.PLAIN_MESSAGE);
  }

  /**
   * Dialog to query a few options from from a drop down menu.
   *
   * @param queryString Question to user.
   * @param menuElements Options to select between.
   * @return Index of the selected element.
   */
  public static int userInputFromMenu(String queryString, String... menuElements) {
    String retStr = (String) JOptionPane.showInputDialog(null,
        queryString, "Choose from menu", JOptionPane.INFORMATION_MESSAGE,
        null, menuElements, menuElements[0]);
    retStr = (retStr == null) ? "" : retStr;
    int menuItemNo = 0;
    int i = 0;
    for (String str : menuElements) {
      if (retStr.equals(str)) {
        menuItemNo = i;
      }
      i++;
    }
    return menuItemNo;
  }

  /**
   * Dialog to get String input from the user.
   *
   * @param queryString Question to user.
   * @return User input.
   */
  public static String userInput(String queryString) {
    String retStr = (String) JOptionPane.showInputDialog(queryString);
    retStr = (retStr == null) ? "" : retStr;
    return retStr;
  }
}
