package games.yatzy.view;

import games.util.GenericObserver;
import games.yatzy.control.GameControl;
import games.yatzy.model.GameState;
import games.yatzy.model.ScoreInterface;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;

/**
 * A panel with player name and scores.
 *
 * @author Jan Eriksson
 * @version 1.0
 * @since 07/12/15
 */
public class PlayerColumn extends JPanel implements GenericObserver<ScoreInterface> {

  private String playerName;
  private boolean isActive;
  private JLabel playerLabel;
  private Map<String, JComponent> scoreColumn;
  private final Dimension preferedLabelSize = new Dimension(60, 20);

  public PlayerColumn(String playerName, ScoreInterface scoreInterface, GameControl gameControl,
                      GameState gameState) {
    List<String> allScores = scoreInterface.getAllScores();
    this.setLayout(new GridLayout(0,1));

    this.playerName = playerName;
    playerLabel = new JLabel(playerName);

    isActive = false;

    playerLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    playerLabel.setHorizontalAlignment(SwingConstants.CENTER);
    playerLabel.setPreferredSize(preferedLabelSize);
    Font font = playerLabel.getFont();
    Font boldFont = new Font(font.getFontName(), Font.BOLD, font.getSize());
    playerLabel.setFont(boldFont);
    this.add(playerLabel);

    scoreColumn = new HashMap<>();
    for (String boxId : allScores) {
      JButton scoreComponent = new JButton(Integer.toString(scoreInterface
          .getScore(playerName, boxId)));

      // Appearance of button to look like a JLabel
      scoreComponent.setFocusPainted(false);
      scoreComponent.setMargin(new Insets(0, 0, 0, 0));
      scoreComponent.setContentAreaFilled(false);
      scoreComponent.setOpaque(false);

      scoreComponent.setForeground(Color.BLACK);
      scoreComponent.setHorizontalTextPosition(JButton.CENTER);
      scoreComponent.setHorizontalAlignment(JButton.CENTER);
      scoreComponent.setBorder(BorderFactory.createEtchedBorder());
      scoreComponent.setPreferredSize(preferedLabelSize);

      this.add(scoreComponent);
      scoreColumn.put(boxId, scoreComponent);

      // For each non-derived score set a one-shot action listener that
      // will run if scoring is allowed and this is the active player.
      // After set score, unregister so no more score setting is allowed
      // on this box.
      if (!scoreInterface.isDerivedScore(boxId)) {
        scoreComponent.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            if (isActive && gameState.isScoringAllowed()) {
              gameControl.setScore(boxId);
              scoreComponent.removeActionListener(this);
            }
          }
        });
      }
    }
    scoreInterface.registerObserver(this);
  }

  /**
   * Update the scores, with either final scores or temp scores.
   *
   * @param scoreInterface Score model.
   */
  @Override
  public void update(ScoreInterface scoreInterface) {
    List<String> allScores = scoreInterface.getAllScores();
    for (String boxId : allScores) {
      JButton button = (JButton) scoreColumn.get(boxId);
      if (scoreInterface.isScoreSet(playerName,boxId)) {
        button.setText(Integer.toString(scoreInterface.getScore(playerName, boxId)));
        button.setForeground(Color.BLACK);
      } else {
        int tmpScore = scoreInterface.getTempScore(playerName,boxId);
        button.setText(Integer.toString(tmpScore));
        if (tmpScore > 0) {
          Color color = new Color(0xA408E9);
          button.setForeground(color);
        } else {
          button.setForeground(Color.BLACK);
        }
      }
      button.setPreferredSize(preferedLabelSize);
    }
  }

  public boolean isActive() {
    return isActive;
  }

  public void setActive(boolean active) {
    isActive = active;
  }

}
